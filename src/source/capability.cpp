/*!*********************************************************************************
 *  \file       capability.cpp
 *  \brief      Capability main file.
 *  \details    This file implements the main function of the Capability.
 *  \authors    Alberto Camporredondo.
 *  \copyright  Copyright 2017 Universidad Politecnica de Madrid (UPM)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ********************************************************************************/

#include "../include/capability.h"

Capability::Capability()
{
  reference_number = 0;
}

Capability::Capability(std::string name, std::vector<std::string> processes)
{
  this->name = name;
  this->processes = processes;
  this->reference_number = 0;
}

Capability::~Capability()
{

}


/* Getter's */
std::string Capability::getName()
{
  return name;
}

std::vector<std::string> Capability::getProcesses()
{
  return processes;
}

int Capability::getReferenceNumber()
{
  return reference_number;
}


/* Setter's */
void Capability::setName(std::string name)
{
  this->name = name;
}

void Capability::setProcesses(std::vector<std::string> processes)
{
  this->processes = processes;
}

void Capability::setReferenceNumber(int number)
{
  this->reference_number = number;
}


/* Functionality */
int Capability::decrementReferences()
{
  return reference_number-=1;
}

int Capability::incrementReferences()
{
  return reference_number+=1;
}


bool Capability::operator==(Capability c) {
  return name == c.getName();
}
