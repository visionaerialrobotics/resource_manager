/*!*********************************************************************************
 *  \file       capability.h
 *  \brief      Capability definition file.
 *  \details    This file contains the Capability declaration.
 *              To obtain more information about it's definition consult
 *              the capability.cpp file.
 *  \authors    Alberto Camporredondo.
 *  \copyright  Copyright 2017 Universidad Politecnica de Madrid (UPM)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ********************************************************************************/

#ifndef CAPABILITY_H
#define CAPABILITY_H

#include <string>
#include <vector>

class Capability
{
public:
  Capability();
  Capability(std::string name, std::vector<std::string> processes);
  ~Capability();

private:
  std::string name;
  int reference_number;
  std::vector<std::string> processes;

public:
  /*Getter's*/
  std::string getName();
  std::vector<std::string> getProcesses();
  int getReferenceNumber();

  /*Setter's*/
  void setName(std::string name);
  void setProcesses(std::vector<std::string> processes);
  void setReferenceNumber(int number);

  /*Functionality*/
  int decrementReferences();
  int incrementReferences();

  bool operator==(Capability c);
};

#endif
